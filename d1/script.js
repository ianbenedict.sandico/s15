// console.log("Hello World");
// console.log("Using JavaScript");

// let name = "Peter Griffin";
// console.log("Hello World");
// console.log("Using Javascript");


// console.log("hello world".length)
// let num1 = 12
// let num2 = 3
// console.log(num1 + num2);

// let x = 2;
// let y = 3;

// console.log(x);
// //expected output: 2

// console.log(x = y + 1) // 3 + 1
// //expected output: 4

// console.log(x = x * y); // 4 * 3
// // expected output: 12

// let bar = 5

// //Number + number -> addition
// bar += 2 //7

// //expected output is 7
// console.log(bar);

// let bar = 5
// bar -= 2
// console.log(bar);

// let bar = 5
// bar *= 2
// console.log(bar);

// let bar = 5
// bar /= 2
// console.log(bar);

// console.log(1 === 1);
// //expected output: true

// console.log("1" === 1);  
// //expected output: false

// console.log(1 == 1);
// //exptected output: true

// console.log("1" == 1);
// //expected output: true

// console.log(1 != 2)
// //expected output: true

// console.log(1 != '1');
// //expected output: true

// console.log(3 !== '3')
// //true

// console.log(4 !== 3)
// //true

// //Relational Operators

// let x = 4;
// let y = 3;

// console.log(x > y);
// //expected: true

// let x = 4;
// let y = 3;

// console.log(x >= y);
// //expected output: true

// console.log(y >= x);
// //expected output: false

// console.log(x < y);
// //expected output false;

// console.log (x <= y);
// //expected output: false

// console.log(x <= 10);
// //expected output: true

// //logic operators


// // Logical AND (&&)

// console.log((19>11) && (23 <50));
// //returns true

// // logical OR (||)
// //1 true = true


// console.log((19>11) || (23 <50));
// //returns true

// console.log((19> 11) || (23 === 50));
// //returns true

// // Logical NOT (!)
// //just reverse true to false and vice versa

// console.log(! (100<150));
// // returns false

// conditional statements

let x = 12;
if(x === 10){
	console.log("Equal to 10");
}else if(x > 10){
	console.log("x is greather than 10");
}
else{
	console.log("is not equal to 10");
}